import { computed } from "vue";

const useRightsidebar = () => {
  const getUserHour = (hour) => computed(() => {});

  const getMultimedia = (data) =>
    computed(() =>
      data.reduce((accum, element) => {
        if (element?.message?.multimedia?.file) {
            accum++
        }
        return accum
      }, 0)
    );

  return {
    getUserHour,
    getMultimedia,
  };
};

export default useRightsidebar;

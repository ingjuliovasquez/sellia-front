import { ref, watch } from "vue";
import axiosInstance from "@/plugins/axiosInstance";

const useChat = () => {
  const loadingChat = ref(false);
  const conversation = ref([]);
  const hasChat = ref(false);

  const setChatId = (id) => {
    chatId.value = id;
  };

  const getConversation = async (id) => {
    try {
      const { data } = await axiosInstance.get(`${id}.json`);
      // console.log(data);

      if(typeof data === 'string') {
        const newDataFormat = JSON.parse(data)
        console.log(typeof newDataFormat);
      }

      hasChat.value = true;
      loadingChat.value = true;
      conversation.value = data;
      loadingChat.value = false;
    } catch (error) {
      console.log(error);
    }
  };

  return {
    hasChat,
    setChatId,
    loadingChat,
    conversation,
    getConversation,
  };
};

export default useChat;

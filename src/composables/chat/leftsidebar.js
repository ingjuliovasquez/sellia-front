import { onMounted, ref } from "vue";
import axiosInstance from "@/plugins/axiosInstance";

const useLeftsidebar = () => {
  const loadingContacts = ref(true);
  const hasChat = ref(false);
  const contacts = ref([]);
  const chatData = ref(null);

  const getContacts = async () => {
    const { data } = await axiosInstance.get("/clients.json");
    contacts.value = data;
  };

  const selectedChat = async (id) => {
    hasChat.value = true;
    chatData.value = contacts.value.find((item) => item._id === id);
    
  };

  onMounted(() => {
    getContacts();
  });

  return { chatData, contacts, loadingContacts, hasChat, selectedChat };
};

export default useLeftsidebar;
